#!/bin/bash

# Mendapatkan waktu saat ini
now=$(date +"%H:%M %d:%m:%Y")

# Membuat nama file backup dengan format jam:menit tanggal:bulan:tahun
backup_file_name=$(date +"%H:%M_%d:%m:%Y").txt

# Membuat direktori backup (Jika belum ada)
if [ ! -d "backup" ]; then
  mkdir backup
fi

# Memindahkan file syslog ke direktori backup
cp /var/log/syslog backup/$backup_file_name

# Menentukan shift cipher
shift=$(date +"%H")

# Membuat string manipulation
alphabet="abcdefghijklmnopqrstuvwxyz"
ciphered_alphabet=$(echo $alphabet | tr "${alphabet:0:26}" "${alphabet:${shift}:26}${alphabet:0:${shift}}")
ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')

# Mengenkripsi isi file log dengan shift cipher yang telah ditentukan
sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" backup/$backup_file_name

echo "File $backup_file_name has been successfully encrypted"

# Pakai cronjobs buat backup file setiap 2 jam
# * 2 * * * sh /home/nas/Documents/sisop/modul_1/soal4/log_encrypt.sh

# Backup file syslog setiap 2 jam
#if [ $(date +"%H") -eq "00" ] || [ $(date +"%H") -eq "02" ] || [ $(date +"%H") -eq "04" ] || [ $(date +"%H") -eq "06" ] || [ $(date +"%H") -eq "08" ] || [ $(date +"%H") -eq "10" ] || [ $(date +"%H") -eq "12" ] || [ $(date +"%H") -eq "14" ] || [ $(date +"%H") -eq "16" ] || [ $(date +"%H") -eq "18" ] || [ $(date +"%H") -eq "20" ] || [ $(date +"%H") -eq "22" ]
#then
#  cp ./backup/$(date +"%H:%M_%d:%m:%Y").txt
#fi
