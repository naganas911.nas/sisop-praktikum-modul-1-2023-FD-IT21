#!/bin/bash

# meminta input dari user
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo ""

# membaca file users.txt
users_file="/users/users.txt"
if [ ! -f "$users_file" ]; then
  echo "Error: $users_file tidak ditemukan"
  exit 1
fi

# mencari baris dengan username yang diinput
user_entry=$(grep "^$username:" "$users_file")

# jika username tidak ditemukan
if [ -z "$user_entry" ]; then
  echo "Error: username tidak ditemukan"
  exit 1
fi

# memeriksa password
saved_password=$(echo "$user_entry" | cut -d':' -f2)
if [ "$password" != "$saved_password" ]; then
  echo "Error: password salah"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> /users/log.txt
  exit 1
fi

# login berhasil
echo "Selamat datang, $username!"
echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> /users/log.txt
exit 0

