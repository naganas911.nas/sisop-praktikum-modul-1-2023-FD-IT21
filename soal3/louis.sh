#!/bin/bash

# Membuat direktori /users dan file users.txt dan log.txt jika belum ada
if [ ! -d "/users" ]; then
  mkdir /users
fi

if [ ! -f "/users/users.txt" ]; then
  touch /users/users.txt
fi

if [ ! -f "/users/log.txt" ]; then
  touch /users/log.txt
fi

# Fungsi untuk validasi password
validate_password() {
  local password=$1
  local username=$2

  # Minimal 8 karakter
  if [ ${#password} -lt 8 ]; then
    echo "Password minimal 8 karakter"
    return 1
  fi

  # Minimal 1 huruf kapital dan 1 huruf kecil
  if ! [[ $password =~ [A-Z] && $password =~ [a-z] ]]; then
    echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    return 1
  fi

  # Alphanumeric
  if ! [[ $password =~ ^[a-zA-Z0-9]+$ ]]; then
    echo "Password harus alphanumeric"
    return 1
  fi

  # Tidak boleh sama dengan username
  if [ "$password" = "$username" ]; then
    echo "Password tidak boleh sama dengan username"
    return 1
  fi

  # Tidak boleh menggunakan kata chicken atau ernie
  if [[ "$password" =~ "chicken" || "$password" =~ "ernie" ]]; then
    echo "Password tidak boleh menggunakan kata 'chicken' atau 'ernie'"
    return 1
  fi

  return 0
}

# Membaca input username dan password dari user
read -p "Masukkan username: " username
read -p "Masukkan password: " -s password
echo ""

# Memvalidasi password
if ! validate_password $password $username; then
  exit 1
fi

# Memeriksa apakah user sudah terdaftar
if grep -q "^$username:" /users/users.txt; then
  echo "REGISTER: ERROR User already exists"
  echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> /users/log.txt
  exit 1
fi

# Menambahkan user baru ke dalam file users.txt
echo "$username:$password" >> /users/users.txt
echo "REGISTER: INFO User $username registered successfully"
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> /users/log.txt

