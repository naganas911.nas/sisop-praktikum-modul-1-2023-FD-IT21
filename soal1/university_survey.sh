#!/bin/bash

bocchi_survey=$(cat '2023 QS World University Rankings.csv')

# poin 1
univ_best=$(echo "$bocchi_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -t ',' -k1 | head -n 5)
echo "Top 5 Universitas di Jepang:"
echo "$univ_best"  | awk -F, '{ print $1 "\t" $2 }'
echo " "

# poin 2
fsr_terendah=$(echo "$univ_best" | sort -n -t ',' -k9 | tail -n 5)
echo "Universitas dengan FSR Score terendah di antara 5 Universitas teratas:"
echo "$fsr_terendah" | awk -F, '{ print $2 "\t" $9 }' 
echo " "

# poin 3
ger_best=$(echo "$bocchi_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -t ',' -k20 | tail -n 10)
echo "Top 10 Universitas di Jepang dengan GER Rank tertinggi:"
echo "$ger_best" | awk -F, '{ print $1 "\t" $2 "\t" $20 }'
echo " "

# poin 4
univ_keren=$(echo "$bocchi_survey" | grep -i "keren")
echo "Universitas paling keren di dunia:"
echo "$univ_keren" | awk -F, '{ print $1 "\t" $2 }'
