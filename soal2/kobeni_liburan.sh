#!/bin/bash

# Membuat folder kumpulan pertama jika belum ada
if [ ! -d "kumpulan_1" ]; then
mkdir kumpulan_1
fi

# Mendapatkan jam saat ini
jam=$(date +%H)

# Mendapatkan nomor folder kumpulan saat ini
folder=$(ls -d kumpulan_* | wc -l)

# Jika sudah 10 jam sejak folder terakhir dibuat, buat folder baru
if [ $((jam%10)) -eq 0 ]; then
folder=$((folder+1))
mkdir "kumpulan_$folder"
fi

# Mendapatkan nomor file gambar terakhir
nomor=$(ls kumpulan_$folder/perjalanan_* 2>/dev/null | wc -l)

# Mendownload gambar sebanyak X kali dengan X adalah jam saat ini
for (( i=1; i<=$jam; i++ )); do
nomor=$((nomor+1))
wget -O "kumpulan_$folder/perjalanan_$nomor.jpg" "https://source.unsplash.com/1600x900/?indonesia"
done

## Membuat folder devil pertama jika belum ada
if [ ! -d "devil_1" ]; then
mkdir devil_1
fi

# Mendapatkan nomor ZIP saat ini
zip=$(ls devil_* | wc -l)

# Jika sudah satu hari sejak ZIP terakhir dibuat, buat ZIP baru
if [ $(find devil_*/ -mtime +1 | wc -l) -gt 0 ]; then
zip=$((zip+1))
zip -r "devil_$zip.zip" "kumpulan_$((zip-1))"
fi
