#!/bin/bash

# Mendapatkan nama file yang akan didekripsi
echo "Enter the name of the file to decrypt (format: HH:MM_dd:mm:yyyy.txt):"
read file_name

# Menentukan shift cipher dari waktu pada nama file
shift=$(echo $file_name | cut -d'_' -f1 | cut -d':' -f1)

# Membuat string manipulation dengan shift cipher yang ditentukan
alphabet="abcdefghijklmnopqrstuvwxyz"
ciphered_alphabet=$(echo $alphabet | tr "${alphabet:${shift}:26}${alphabet:0:${shift}}" "${alphabet:0:26}")
ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')

# Mendekripsi isi file dengan shift cipher yang telah ditentukan
sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" $file_name

echo "File $file_name has been successfully decrypted."
