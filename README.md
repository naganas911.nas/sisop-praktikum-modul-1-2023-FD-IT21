# Analisis Cara Kerja
## SOAL 1

### Source Code 

```
#!/bin/bash

bocchi_survey=$(cat '2023 QS World University Rankings.csv')

# poin 1
univ_best=$(echo "$bocchi_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -t ',' -k1 | head -n 5)
echo "Top 5 Universitas di Jepang:"
echo "$univ_best"  | awk -F, '{ print $1 "\t" $2 }'
echo " "

# poin 2
fsr_terendah=$(echo "$univ_best" | sort -n -t ',' -k9 | tail -n 5)
echo "Universitas dengan FSR Score terendah di antara 5 Universitas teratas:"
echo "$fsr_terendah" | awk -F, '{ print $2 "\t" $9 }' 
echo " "

# poin 3
ger_best=$(echo "$bocchi_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -t ',' -k20 | tail -n 10)
echo "Top 10 Universitas di Jepang dengan GER Rank tertinggi:"
echo "$ger_best" | awk -F, '{ print $1 "\t" $2 "\t" $20 }'
echo " "

# poin 4
univ_keren=$(echo "$bocchi_survey" | grep -i "keren")
echo "Universitas paling keren di dunia:"
echo "$univ_keren" | awk -F, '{ print $1 "\t" $2 }'
```

### Penjelasan
- Variabel `bocchi_survey` diisi dengan isi file CSV yang dibaca menggunakan perintah `cat`.
- Variabel `univ_best` diisi dengan lima baris data universitas terbaik di Jepang yang diurutkan berdasarkan peringkat QS World University Rankings 2023. Data tersebut dipilih menggunakan perintah `awk`, yang hanya memilih baris yang memiliki kode negara 'JP'. Selanjutnya, baris-baris tersebut diurutkan berdasarkan peringkat, dan lima baris teratas dipilih menggunakan perintah `head`. Variabel tersebut kemudian dicetak ke layar menggunakan perintah `echo`.
- Variabel `fsr_terendah` diisi dengan lima baris data universitas dengan skor FSR (Faculty-Student Ratio) terendah di antara lima universitas terbaik di Jepang yang sudah diambil pada poin sebelumnya. Data tersebut diurutkan berdasarkan skor FSR dan lima baris terbawah dipilih menggunakan perintah `tail`. Variabel tersebut kemudian dicetak ke layar menggunakan perintah `echo`.
- Variabel `ger_best` diisi dengan sepuluh baris data universitas terbaik di Jepang berdasarkan GER (Graduate Employability Rankings) Rank. Data tersebut dipilih menggunakan perintah `awk`, yang hanya memilih baris yang memiliki kode negara 'JP'. Selanjutnya, baris-baris tersebut diurutkan berdasarkan peringkat GER Rank, dan sepuluh baris teratas dipilih menggunakan perintah `tail`. Variabel tersebut kemudian dicetak ke layar menggunakan perintah `echo`.
- Variabel `univ_keren` diisi dengan baris data universitas yang memiliki kata 'keren' di dalamnya, baik itu dalam kolom nama universitas maupun dalam kolom lokasi universitas. Data tersebut dipilih menggunakan perintah `grep`. Variabel tersebut kemudian dicetak ke layar menggunakan perintah `echo`.

Dalam program tersebut, dilakukan penggunaan beberapa perintah seperti `awk`, `sort`, `head`, `tail`, dan `grep` untuk melakukan pemrosesan data dan pengambilan informasi yang dibutuhkan. Setelah itu, hasil pengolahan data ditampilkan ke layar menggunakan perintah `echo` dan `awk`.

## SOAL 2

### Source Code
```
#!/bin/bash

# Membuat folder kumpulan pertama jika belum ada
if [ ! -d "kumpulan_1" ]; then
mkdir kumpulan_1
fi

# Mendapatkan jam saat ini
jam=$(date +%H)

# Mendapatkan nomor folder kumpulan saat ini
folder=$(ls -d kumpulan_* | wc -l)

# Jika sudah 10 jam sejak folder terakhir dibuat, buat folder baru
if [ $((jam%10)) -eq 0 ]; then
folder=$((folder+1))
mkdir "kumpulan_$folder"
fi

# Mendapatkan nomor file gambar terakhir
nomor=$(ls kumpulan_$folder/perjalanan_* 2>/dev/null | wc -l)

# Mendownload gambar sebanyak X kali dengan X adalah jam saat ini
for (( i=1; i<=$jam; i++ )); do
nomor=$((nomor+1))
wget -O "kumpulan_$folder/perjalanan_$nomor.jpg" "https://source.unsplash.com/1600x900/?indonesia"
done

## Membuat folder devil pertama jika belum ada
if [ ! -d "devil_1" ]; then
mkdir devil_1
fi

# Mendapatkan nomor ZIP saat ini
zip=$(ls devil_* | wc -l)

# Jika sudah satu hari sejak ZIP terakhir dibuat, buat ZIP baru
if [ $(find devil_*/ -mtime +1 | wc -l) -gt 0 ]; then
zip=$((zip+1))
zip -r "devil_$zip.zip" "kumpulan_$((zip-1))"
fi
```
### Penjelasan

- Pertama-tama, program akan memeriksa apakah folder "kumpulan_1" sudah ada. Jika belum, maka program akan membuat folder tersebut menggunakan perintah `mkdir`.
- Program selanjutnya akan mendapatkan jam saat ini menggunakan perintah date +%H, dan mendapatkan nomor folder kumpulan saat ini dengan menghitung jumlah folder yang dimulai dengan "kumpulan_" menggunakan perintah `ls -d kumpulan_* | wc -l`.
- Jika sudah 10 jam sejak folder terakhir dibuat (yaitu jika nilai jam saat ini habis dibagi 10), maka program akan membuat folder baru dengan nomor yang diincrement dari folder sebelumnya menggunakan perintah `mkdir`.
- Program selanjutnya akan mendapatkan nomor file gambar terakhir dalam folder kumpulan saat ini menggunakan perintah `ls kumpulan_$folder/perjalanan_* 2>/dev/null | wc -l`. Jika tidak ada file dalam folder kumpulan saat ini, maka nilai nomor akan di-set ke 0.
- Program akan mendownload gambar sebanyak X kali, dimana X adalah jam saat ini. Program akan menggunakan perintah `wget` untuk mendownload gambar dengan ukuran 1600x900 piksel dari website "https://source.unsplash.com/1600x900/?indonesia". Setiap gambar akan diberi nama "perjalanan_N.jpg", dimana N adalah nomor file yang diincrement dari file terakhir yang ada dalam folder kumpulan saat ini.
- Program selanjutnya akan memeriksa apakah folder "devil_1" sudah ada. Jika belum, maka program akan membuat folder tersebut menggunakan perintah `mkdir`.
- Program akan mendapatkan nomor file ZIP saat ini dengan menghitung jumlah folder yang dimulai dengan "devil_" menggunakan perintah `ls devil_* | wc -l`.
- Jika sudah satu hari sejak file ZIP terakhir dibuat (yaitu jika ada file dalam folder "devil_" yang lebih tua dari satu hari menggunakan perintah `find devil_*/ -mtime +1 | wc -l`), maka program akan membuat file ZIP baru dengan nama "devil_N.zip", dimana N adalah nomor file yang diincrement dari file terakhir yang ada dalam folder "devil_". File ZIP baru akan mengemas seluruh file dalam folder kumpulan sebelumnya menggunakan perintah `zip -r`.

## SOAL 3

### Source Code Register
```
#!/bin/bash

# Membuat direktori /users dan file users.txt dan log.txt jika belum ada
if [ ! -d "/users" ]; then
  mkdir /users
fi

if [ ! -f "/users/users.txt" ]; then
  touch /users/users.txt
fi

if [ ! -f "/users/log.txt" ]; then
  touch /users/log.txt
fi

# Fungsi untuk validasi password
validate_password() {
  local password=$1
  local username=$2

  # Minimal 8 karakter
  if [ ${#password} -lt 8 ]; then
    echo "Password minimal 8 karakter"
    return 1
  fi

  # Minimal 1 huruf kapital dan 1 huruf kecil
  if ! [[ $password =~ [A-Z] && $password =~ [a-z] ]]; then
    echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    return 1
  fi

  # Alphanumeric
  if ! [[ $password =~ ^[a-zA-Z0-9]+$ ]]; then
    echo "Password harus alphanumeric"
    return 1
  fi

  # Tidak boleh sama dengan username
  if [ "$password" = "$username" ]; then
    echo "Password tidak boleh sama dengan username"
    return 1
  fi

  # Tidak boleh menggunakan kata chicken atau ernie
  if [[ "$password" =~ "chicken" || "$password" =~ "ernie" ]]; then
    echo "Password tidak boleh menggunakan kata 'chicken' atau 'ernie'"
    return 1
  fi

  return 0
}

# Membaca input username dan password dari user
read -p "Masukkan username: " username
read -p "Masukkan password: " -s password
echo ""

# Memvalidasi password
if ! validate_password $password $username; then
  exit 1
fi

# Memeriksa apakah user sudah terdaftar
if grep -q "^$username:" /users/users.txt; then
  echo "REGISTER: ERROR User already exists"
  echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> /users/log.txt
  exit 1
fi

# Menambahkan user baru ke dalam file users.txt
echo "$username:$password" >> /users/users.txt
echo "REGISTER: INFO User $username registered successfully"
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> /users/log.txt
```
### Penjelasan
- Pertama,Program ini akan membuat direktori `/users` dan file `users.txt` serta `log.txt` jika belum ada, kemudian meminta input dari pengguna berupa username dan password.
- Setelah itu, program akan memvalidasi password menggunakan fungsi `validate_password()`, yang memeriksa beberapa kriteria seperti minimal 8 karakter, minimal 1 huruf kapital dan 1 huruf kecil, alphanumeric, tidak boleh sama dengan username, dan tidak boleh menggunakan kata tertentu seperti "chicken" atau "ernie".
- Jika password tidak memenuhi kriteria, program akan mengeluarkan pesan kesalahan dan keluar dengan status 1. Jika password memenuhi kriteria, program akan memeriksa apakah user sudah terdaftar atau belum dengan memeriksa file `users.txt`.
- Jika user sudah terdaftar, program akan mengeluarkan pesan kesalahan dan keluar dengan status 1. Jika user belum terdaftar, program akan menambahkan user baru ke dalam file `users.txt`, mengeluarkan pesan sukses, dan menambahkan pesan log ke dalam file `log.txt`.
- Pesan log yang ditambahkan ke dalam file `log.txt` akan berisi informasi tentang waktu pendaftaran dan apakah pendaftaran berhasil atau tidak. Pesan log ini akan berguna untuk memantau aktivitas pengguna dalam sistem.

### Source Code Login
```
#!/bin/bash

# meminta input dari user
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo ""

# membaca file users.txt
users_file="/users/users.txt"
if [ ! -f "$users_file" ]; then
  echo "Error: $users_file tidak ditemukan"
  exit 1
fi

# mencari baris dengan username yang diinput
user_entry=$(grep "^$username:" "$users_file")

# jika username tidak ditemukan
if [ -z "$user_entry" ]; then
  echo "Error: username tidak ditemukan"
  exit 1
fi

# memeriksa password
saved_password=$(echo "$user_entry" | cut -d':' -f2)
if [ "$password" != "$saved_password" ]; then
  echo "Error: password salah"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> /users/log.txt
  exit 1
fi

# login berhasil
echo "Selamat datang, $username!"
echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> /users/log.txt
exit 0
```
### Penjelasan

- `read -p "Masukkan username: " username` : meminta input dari user untuk memasukkan username.
- `read -s -p "Masukkan password: " password` : meminta input dari user untuk memasukkan password dengan opsi -s agar input password tidak ditampilkan pada layar.
- `echo ""` : menambahkan baris baru pada output.
- `users_file="/users/users.txt"` : menyimpan path dari file users.txt pada variabel `users_file`.
- `if [ ! -f "$users_file" ]; then` : melakukan pengecekan apakah file users.txt ada atau tidak dengan menggunakan perintah `[ ! -f "$users_file" ]`. Jika file tidak ditemukan, maka program akan menampilkan pesan error dan keluar dari program dengan menggunakan perintah `exit 1`.
- `user_entry=$(grep "^$username:" "$users_file") :` mencari baris pada file users.txt yang mengandung username yang dimasukkan oleh user menggunakan perintah `grep`. Hasil pencarian disimpan pada variabel `user_entry`.
- `if [ -z "$user_entry" ]; then` : melakukan pengecekan apakah hasil pencarian pada file users.txt menghasilkan baris kosong atau tidak menggunakan perintah `[ -z "$user_entry" ]`. Jika hasil pencarian kosong, maka program akan menampilkan pesan error dan keluar dari program dengan menggunakan perintah `exit 1`.
- `saved_password=$(echo "$user_entry" | cut -d':' -f2)` : mengambil password yang tersimpan pada baris hasil pencarian dengan menggunakan perintah `cut`.
- `if [ "$password" != "$saved_password" ]; then` : melakukan pengecekan apakah password yang dimasukkan oleh user sama dengan password yang tersimpan pada file users.txt menggunakan perintah `[ "$password" != "$saved_password" ]`. Jika password tidak sama, maka program akan menampilkan pesan error, menuliskan log pada file log.txt, dan keluar dari program dengan menggunakan perintah `exit 1`.
- `echo "Selamat datang, $username!"` : menampilkan pesan selamat datang dengan username pada layar.
- `echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> /users/log.txt` : menuliskan log bahwa user berhasil login ke dalam file log.txt dengan menggunakan perintah `>>` agar data ditambahkan pada akhir file log.txt.
- `exit 0` : keluar dari program dengan menggunakan perintah `exit 0` yang menunjukkan bahwa program berhasil dijalankan dengan sukses.

## SOAL 4

### Source Code Encrypt
```
#!/bin/bash

# Mendapatkan waktu saat ini
now=$(date +"%H:%M %d:%m:%Y")

# Membuat nama file backup dengan format jam:menit tanggal:bulan:tahun
backup_file_name=$(date +"%H:%M_%d:%m:%Y").txt

# Membuat direktori backup (Jika belum ada)
if [ ! -d "backup" ]; then
  mkdir backup
fi

# Memindahkan file syslog ke direktori backup
cp /var/log/syslog backup/$backup_file_name

# Menentukan shift cipher
shift=$(date +"%H")

# Membuat string manipulation
alphabet="abcdefghijklmnopqrstuvwxyz"
ciphered_alphabet=$(echo $alphabet | tr "${alphabet:0:26}" "${alphabet:${shift}:26}${alphabet:0:${shift}}")
ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')

# Mengenkripsi isi file log dengan shift cipher yang telah ditentukan
sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" backup/$backup_file_name

echo "File $backup_file_name has been successfully encrypted"

# Pakai cronjobs buat backup file setiap 2 jam
# * 2 * * * sh /home/nas/Documents/sisop/modul_1/soal4/log_encrypt.sh

# Backup file syslog setiap 2 jam
#if [ $(date +"%H") -eq "00" ] || [ $(date +"%H") -eq "02" ] || [ $(date +"%H") -eq "04" ] || [ $(date +"%H") -eq "06" ] || [ $(date +"%H") -eq "08" ] || [ $(date +"%H") -eq "10" ] || [ $(date +"%H") -eq "12" ] || [ $(date +"%H") -eq "14" ] || [ $(date +"%H") -eq "16" ] || [ $(date +"%H") -eq "18" ] || [ $(date +"%H") -eq "20" ] || [ $(date +"%H") -eq "22" ]
#then
#  cp ./backup/$(date +"%H:%M_%d:%m:%Y").txt
#fi
```
### Penjelasan

- Mendapatkan waktu saat ini dengan menggunakan perintah `date +"%H:%M %d:%m:%Y"`. Format waktu yang digunakan adalah `jam:menit tanggal:bulan:tahun`.
- Membuat nama file backup dengan format yang sama seperti waktu saat ini dengan menggunakan perintah `date +"%H:%M_%d:%m:%Y".txt`. Nama file backup disimpan dalam variabel `backup_file_name`.
- Membuat direktori backup jika belum ada dengan menggunakan perintah `if [ ! -d "backup" ]; then mkdir backup; fi`.
- Memindahkan file syslog dari direktori `/var/log` ke direktori backup dengan menggunakan perintah `cp /var/log/syslog backup/$backup_file_name`.
- Menentukan shift cipher untuk enkripsi dengan menggunakan perintah `shift=$(date +"%H")`. Shift cipher dihitung dari jam saat ini.
- Membuat string manipulation dengan shift cipher yang telah ditentukan. Untuk menghasilkan string ciphered_alphabet yang baru, digunakan perintah `ciphered_alphabet=$(echo $alphabet | tr "${alphabet:0:26}" "${alphabet:${shift}:26}${alphabet:0:${shift}}")`. Untuk menghasilkan string ciphered_alphabet_upper yang berisi string ciphered_alphabet dalam huruf besar, digunakan perintah `ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')`.
- Mengenkripsi isi file syslog dengan shift cipher yang telah ditentukan menggunakan perintah `sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" backup/$backup_file_name`. Perintah ini akan mengubah setiap karakter yang ada pada file syslog dengan karakter pada string `ciphered_alphabet` atau `ciphered_alphabet_upper` yang memiliki index yang sama pada string `alphabet`.
- Menampilkan pesan bahwa file backup telah berhasil dienkripsi dengan menggunakan perintah `echo "File $backup_file_name has been successfully encrypted"`

### Source Code Decrypt
```
#!/bin/bash

# Mendapatkan nama file yang akan didekripsi
echo "Enter the name of the file to decrypt (format: HH:MM_dd:mm:yyyy.txt):"
read file_name

# Menentukan shift cipher dari waktu pada nama file
shift=$(echo $file_name | cut -d'_' -f1 | cut -d':' -f1)

# Membuat string manipulation dengan shift cipher yang ditentukan
alphabet="abcdefghijklmnopqrstuvwxyz"
ciphered_alphabet=$(echo $alphabet | tr "${alphabet:${shift}:26}${alphabet:0:${shift}}" "${alphabet:0:26}")
ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')

# Mendekripsi isi file dengan shift cipher yang telah ditentukan
sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" $file_name

echo "File $file_name has been successfully decrypted."
```
### Penjelasan
- Mendapatkan nama file yang akan didekripsi dari input pengguna dengan menggunakan perintah `read`.
- Menentukan shift cipher dengan mengambil jam dari waktu pada nama file. Caranya adalah dengan memotong string nama file pada bagian jam (HH) menggunakan `cut` dan menyimpannya dalam variabel `shift`.
- Membuat string alphabet biasa dan alphabet yang sudah di-shift dengan shift cipher yang telah ditentukan. Caranya adalah dengan menggunakan perintah `tr` yang mengganti setiap karakter pada alphabet biasa dengan karakter pada alphabet yang sudah di-shift.
- Mendekripsi isi file dengan shift cipher yang telah ditentukan. Caranya adalah dengan menggunakan perintah `sed` yang mengganti setiap karakter pada file dengan karakter pada alphabet yang sudah di-shift.
- Menampilkan pesan bahwa file telah berhasil didekripsi.
